package costavr.utils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

public class DeepCloner {

    private static Logger logger = Logger.getLogger(DeepCloner.class);;

    public static Object deepClone(Object toClone) {
        Class<? extends Object> clazz = toClone.getClass();
        Object toRet = null;
        try {
            toRet = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            logger.fatal("EXCEPTION", e);
            return null;
        }
        Field[] fields = clazz.getFields();
        for (Field f : fields) {
            f.setAccessible(true);
            try {
                f.setInt(f, f.getModifiers() & ~Modifier.FINAL);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                logger.fatal("EXCEPTION", e);
                return null;
            }
            Object val = null;
            try {
                val = f.get(f);
            } catch (IllegalArgumentException | IllegalAccessException e1) {
                logger.fatal("EXCEPTION", e1);
                return null;
            }
            if (val instanceof Serializable) {
                Object newVal = SerializationUtils.clone((Serializable) val);
                try {
                    toRet.getClass().getField(f.getName()).set(toRet, newVal);
                } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
                    logger.fatal("EXCEPTION", e);
                    return null;
                }
            } else {
                logger.error("Field " + f.getName() + " cannot be cloned --> setted to null");
            }

        }
        return toRet;
    }
}
