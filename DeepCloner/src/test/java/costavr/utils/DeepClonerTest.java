package costavr.utils;

import java.io.Serializable;

import org.junit.Assert;
import org.junit.Test;

public class DeepClonerTest {

    @Test
    public void testdeepClone() {
        Test01 tst01 = new Test01();
            Assert.assertNotNull(DeepCloner.deepClone(tst01));


    }

    private class Test01 implements Serializable {

        private static final long serialVersionUID = 5914067205509612812L;
        private String            f01;
        private final String      f02              = "pippo";
        
        public Test01(){
            
        }
    }
}
